# `llm_prompter`

`llm_prompter` creates easy-to-use Python callable objects from ChatGPT prompts.
See the files in `demos/` for example usage.
