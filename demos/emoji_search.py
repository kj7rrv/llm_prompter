#!/usr/bin/env python3
import argparse
import llm_prompter


find_emojis = llm_prompter.LLMFunction(
    "Suggest some emoji sequences relevant to the query. Encode emojis like this: ✅",
    llm_prompter.Dictionary(
        query=llm_prompter.String("the query"),
        count=llm_prompter.Integer("the number of emoji sequences to generate"),
    ),
    llm_prompter.List(llm_prompter.String("an emoji sequence")),
)


parser = argparse.ArgumentParser()
parser.add_argument("query")
parser.add_argument("--count", "-c", type=int, default=5)
args = parser.parse_args()

print(find_emojis({"query": args.query, "count": args.count}))
